package INF101.lab2;
import java.util.ArrayList;
import java.util.NoSuchElementException;
import java.util.List;

public class Fridge implements IFridge {
    ArrayList <FridgeItem> contentsOfFridge;

    public Fridge (){
        this.contentsOfFridge = new ArrayList <>();
    }

    public int nItemsInFridge(){
        return this.contentsOfFridge.size();
    }
    public int totalSize(){
        return 20;
    }
    public boolean placeIn(FridgeItem item){
        if (this.nItemsInFridge() < this.totalSize()){
            this.contentsOfFridge.add(item);
            return true;
        }
        else return false;
    }
    public void takeOut(FridgeItem item){
        if (this.contentsOfFridge.contains(item)){
            this.contentsOfFridge.remove(item);
        }
        else throw new NoSuchElementException();
    }
    public void emptyFridge(){
        this.contentsOfFridge = new ArrayList <FridgeItem>();
    }
    public List<FridgeItem> removeExpiredFood(){
        List <FridgeItem> expiredItems = new ArrayList <>();
        for (int i = 0; i < contentsOfFridge.size(); i++){
            FridgeItem currentItem = contentsOfFridge.get(i);
            if (currentItem.hasExpired()){
                expiredItems.add(currentItem);
                contentsOfFridge.remove(currentItem);
                i--;
            }
        }
        return expiredItems;
    }
}
